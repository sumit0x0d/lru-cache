#ifndef LRU_CACHE_H
#define LRU_CACHE_H

#include <stddef.h>

#include "doubly-linked-list/doubly-linked-list.h"
#include "hash-table/hash-table.h"

typedef struct {
	DoublyLinkedList *linkedList;
	HashTable *hashTable;
	size_t capacity;
	size_t size;
} LruCache;

LruCache *LruCache_Create(size_t dataSize, size_t capacity, size_t (*hash)(void *key, size_t bucketCount),
	int (*compare)(void *data1, void *data2));
void LruCache_Insert(LruCache* cache, void *data);
void *LruCache_Search(LruCache* cache, void *data);

#endif
