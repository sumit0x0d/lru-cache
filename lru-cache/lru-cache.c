#include <assert.h>
#include <stdlib.h>
#include <string.h>

#include "lru-cache.h"

LruCache *LruCache_Create(size_t dataSize, size_t capacity, size_t (*hash)(void *key, size_t bucketCount),
	int (*compare)(void *data1, void *data2))
{
	LruCache *cache = (LruCache *)malloc(sizeof (LruCache));
	assert(cache);
	cache->linkedList = DoublyLinkedList_Create(dataSize);
	cache->hashTable = HashTable_Create(dataSize, capacity * capacity, hash, compare);
	cache->capacity = capacity;
	cache->size = 0;
	return cache;
}

void LruCache_Insert(LruCache *cache, void *data)
{
	HT_Pair *pair = HashTable_Search(cache->hashTable, data);
	if (pair) {
		DoublyLinkedList_Remove(cache->linkedList, pair->value);
	}
	DoublyLinkedList_PushHead(cache->linkedList, pair->key);
	HashTable_Insert(cache->hashTable, data, DoublyLinkedList_GetHead(cache->linkedList));
	if (cache->size == cache->capacity) {
		DLL_Node *nodeTail = DoublyLinkedList_GetTail(cache->linkedList);
		HashTable_Remove(cache->hashTable, nodeTail->data);
		DoublyLinkedList_PopTail(cache->linkedList);
		return;
	}
	cache->size++;
}

void *LruCache_Search(LruCache *cache, void *data)
{
	return HashTable_Search(cache->hashTable, data);
}
