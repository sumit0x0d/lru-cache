#ifndef HASH_TABLE_H
#define HASH_TABLE_H

#include <stddef.h>

#include "pair/pair.h"

typedef struct HashTable {
	HT_Pair *base;
	size_t keySize;
	size_t bucketCount;
	size_t size;
	size_t (*hash)(void *key, size_t bucketCount);
	int (*compare)(void *data1, void *data2);
} HashTable;

HashTable *HashTable_Create(size_t keySize, size_t bucketCount, size_t (*hash)(void *key, size_t bucketCount),
	int (*compare)(void *data1, void *data2));
void HashTable_Destroy(HashTable *hashTable);
HT_Pair *HashTable_Search(HashTable *hashTable, void *key);
void HashTable_Insert(HashTable *hashTable, void *key, DLL_Node *value);
void HashTable_Remove(HashTable *hashTable, void *key);

#endif
