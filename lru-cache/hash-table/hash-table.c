#include <assert.h>
#include <stdlib.h>

#include "hash-table.h"

#include "pair/pair.h"

HashTable *HashTable_Create(size_t keySize, size_t bucketCount, size_t (*hash)(void *key, size_t bucketCount),
	int (*compare)(void *data1, void *data2))
{
	HashTable *hashTable = (HashTable *)malloc(sizeof (HashTable));
	assert(hashTable);

	hashTable->base = (HT_Pair *)malloc(bucketCount * sizeof (HT_Pair));
	assert(hashTable->base);

	hashTable->keySize = keySize;
	hashTable->bucketCount = bucketCount;
	hashTable->size = 0;
	hashTable->hash = hash;
	hashTable->compare = compare;

	return hashTable;
}

void HashTable_Destroy(HashTable *hashTable)
{
	for (size_t i = 0; i < hashTable->bucketCount; i++) {
		HT_Pair *pair = hashTable->base + i;
		while (pair->next) {
			if (pair->key) {
				HT_Pair_Destroy(pair);
			}
			pair = pair->next;
		}
	}
	free(hashTable->base);
	free(hashTable);
}

HT_Pair *HashTable_Search(HashTable *hashTable, void *key)
{
	size_t index = hashTable->hash(key, hashTable->bucketCount);
	if (!hashTable->base[index].key) {
		return NULL;
	}
	return hashTable->base + index;
}

void HashTable_Insert(HashTable *hashTable, void *key, DLL_Node *value)
{
	HT_Pair *pair = HT_Pair_Create(key, hashTable->keySize, value);
	size_t index = hashTable->hash(key, hashTable->bucketCount);
	if (!hashTable->compare(key, hashTable->base[index].key)) {
		hashTable->base[index].value = pair->value;
	} else {
		HT_Pair *pairPrevious = hashTable->base + index;
		while (pairPrevious->next) {
			pairPrevious = pairPrevious->next;
		}
		pairPrevious->next = pair;
	}
	hashTable->size++;
}

void HashTable_Remove(HashTable *hashTable, void *key)
{
	size_t index = hashTable->hash(key, hashTable->bucketCount);
	if (!hashTable->compare(key, hashTable->base[index].key)) {
		HT_Pair_Destroy(hashTable->base[index].key);
	} else {
		HT_Pair *pair = hashTable->base + index;
		HT_Pair *pairPrevious = pair;
		while (pair->next) {
			if (!hashTable->compare(key, pair->key)) {
				pairPrevious->next = pair->next;
				HT_Pair_Destroy(hashTable->base[index].key);
				hashTable->size--;
				break;
			}
			pairPrevious = pair;
			pair = pair->next;
		}
	}
}
