#ifndef HASH_TABLE_PAIR_H
#define HASH_TABLE_PAIR_H

#include <stddef.h>

#include "../../doubly-linked-list/node/node.h"

typedef struct HT_Pair {
	void *key;
	DLL_Node *value;
	struct HT_Pair *next;
} HT_Pair;

HT_Pair* HT_Pair_Create(void *key, size_t keySize, DLL_Node *value);
void HT_Pair_Destroy(HT_Pair *pair);

#endif
