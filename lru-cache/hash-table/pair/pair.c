#include <assert.h>
#include <stdlib.h>
#include <string.h>

#include "pair.h"

HT_Pair *HT_Pair_Create(void *key, size_t keySize, DLL_Node *value)
{
	HT_Pair *pair = (HT_Pair *)malloc(sizeof (HT_Pair));
	assert(pair);
	pair->key = malloc(keySize);
	assert(pair->key);
	memcpy(pair->key, key, keySize);
	pair->value = value;
	pair->next = NULL;
	return pair;
}

void HT_Pair_Destroy(HT_Pair *pair)
{
	free(pair->key);
	free(pair);
}
