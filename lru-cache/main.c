#include "lru-cache.h"

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int compareInt(void *data1, void *data2)
{
	if (*(int *)data1 < *(int *)data2) {
		return -1;
	} else if (*(int *)data1 > *(int *)data2) {
		return 1;
	}
	return 0;
}

size_t hashInt(void *data, size_t bucketCount)
{
	return *(int *)data % bucketCount;
}

int main(void)
{
	srand((int)time(NULL));
	int x = 10;
	LruCache *cache = LruCache_Create(sizeof (int), 100, hashInt, compareInt);
	LruCache_Insert(cache, &x);
	return 0;
}
