#ifndef DOUBLY_LINKED_LIST_NODE_H
#define DOUBLY_LINKED_LIST_NODE_H

#include <stddef.h>

typedef struct DLL_Node {
	void *data;
	struct DLL_Node *previous;
	struct DLL_Node *next;
} DLL_Node;

DLL_Node *DLL_Node_Create(void *data, size_t dataSize);
void DLL_Node_Destroy(DLL_Node *node);

#endif
