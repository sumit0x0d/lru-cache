#include <assert.h>
#include <stdlib.h>
#include <string.h>

#include "node.h"

DLL_Node *DLL_Node_Create(void *data, size_t dataSize)
{
	DLL_Node *node = (DLL_Node *)malloc(sizeof (DLL_Node));
	assert(node);
	node->data = malloc(dataSize);
	assert(node->data);
	memcpy(node->data, data, dataSize);
	return node;
}

void DLL_Node_Destroy(DLL_Node* node)
{
	free(node->data);
	free(node);
}
