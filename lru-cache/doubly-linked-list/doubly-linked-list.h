#ifndef DOUBLY_LINKED_LIST_H
#define DOUBLY_LINKED_LIST_H

#include <stddef.h>

#include "node/node.h"

typedef struct DoublyLinkedList {
	DLL_Node *head;
	DLL_Node *tail;
	size_t dataSize;
	size_t size;
} DoublyLinkedList;

DoublyLinkedList *DoublyLinkedList_Create(size_t dataSize);
void DoublyLinkedList_Destroy(DoublyLinkedList *linkedList);
DLL_Node *DoublyLinkedList_GetHead(DoublyLinkedList *linkedList);
DLL_Node *DoublyLinkedList_GetTail(DoublyLinkedList *linkedList);
void DoublyLinkedList_PushHead(DoublyLinkedList *linkedList, void *data);
void DoublyLinkedList_PushTail(DoublyLinkedList *linkedList, void *data);
void DoublyLinkedList_PopHead(DoublyLinkedList *linkedList);
void DoublyLinkedList_PopTail(DoublyLinkedList *linkedList);
void DoublyLinkedList_Remove(DoublyLinkedList *linkedList, DLL_Node *node);
void DoublyLinkedList_Traverse(DoublyLinkedList *linkedList, void (*function)(void *data));

#endif
